# Oxford iSpeaker
An interactive tool designed for learners of English who want to speak more accurately and fluently in a variety of situations.

**==Also check out [iWriter](http://github.com/yell0wsuit/iwriter) that helps learners write effectively in English.==**

Visit the webiste to use the tool online: https://yell0wsuit.github.io/ispeaker/

![](https://i.imgur.com/2e21d6X.png)

The tool is also available for premium users on the **Oxford Learner's Dictionaries** website. On December 31st, 2021, this old design was deprecated in favor of new design. This reposity is created to preserve this version, which works independently without relying on the **Oxford Learner's Dictionaries** website's resources. The new design, however, requires online server to function.

More information can be found in the [Help](https://yell0wsuit.github.io/ispeaker/help.html) section.

## Features
- Rewritten the interface using Bootstrap 5. Fully compatible with dark theme.
- Replaced PNG icons with SVG. Courtesy of https://www.svgrepo.com/.
- Rewritten the Help section in clean HTML format for smaller file size (it was bloated before due to Microsoft Word -> HTML).
- Updated videos.
- De-bloated the tool by removing unnecessary images and scripts.

Please note that due to interface changes, this tool is no longer compatible with old browsers (Internet Explorer 11 for example).

## Browser compatibility
Work on the latest version of the following browsers:

- Chromium browsers (Microsoft Edge, Chrome, Opera, Brave, ...)
- Mozilla Firefox (in Private Browsing there're some issues)
- Safari (macOS, iOS, iPadOS)

## Known bugs
- Recording and playback is disabled due to unimplemented code.
- In Mozilla Firefox, there're some issues with saving/loading the review section when using Private Browsing. This is a known bug by the browser itself.
